; Dance Simulator mit Zwei Modi: Ein Zufalls generator und ein Modus, um vorher festgelegte Muster abzuspielen

	EQU	RAND, 0x20	
	CSEG	At 0H		
	jmp	init
	ORG	100H		

;-----------MAIN-----------------------------------
init:
;---------Random Modus aktivieren --------
	;MOV	RAND, #0187d	; Random Modus: Gebe deinen Wumba-Code an (zwischen #00h - #0ffh)
	;jmp Generator 		
;---------Lets Dance Modus aktivieren----
	jmp KingJulien 		

;------- Definiertes Lied aus der Datenbank (Beispiel: I Like To Move It) -----------
KingJulien:
	mov R6, #0b
LetsDance: 
	mov R5, #0b
	mov	DPTR, #moveIt ; Ersetze durch beliebige andere Datenbank
	mov	a, R6
	movc	a, @a+dptr
	clr	C
	add	A, #0fch; 252 -> 4 (right)
	jnc	letsDanceNotRight
	call	showRightArrow
	jmp	LetsDanceRepeat
	ret 
LetsDanceNotRight:
	add	A, #01h; 253 -> 3 (left)
	jnc	letsDanceNotLeft
	call	showLeftArrow
	jmp	LetsDanceRepeat
LetsDanceNotLeft:
	add	A, #01h; 254 -> 2 (down)
	jnc	letsDanceNotDown
	call	showDownArrow
	jmp	LetsDanceRepeat	
LetsDanceNotDown:
	call	showUpArrow
	jmp	LetsDanceRepeat	
	
LetsDanceRepeat:
	inc R6
	cjne	R6, #05h, LetsDance
	jmp KingJulien
	
Generator:
;----- Generiert ein Zufälliges Muster (in Abhängigkeit von der Zahl RAND, die in init gesetzt wurde)----------
	call	RANDOM		;Zufallszahl A bestimmen zwischen 00h und ffh
	mov	R5, #0b
	clr	C
	add	A, #040h
	jnc	notLeft
	call	showleftarrow
	inc	0x28
	jmp	Generator

notLeft:
	add	A, #040h
	jnc	notRight
	call	showrightarrow
	inc	0x29
	jmp	Generator

notRight:
	add	A, #040h
	jnc	notUp
	call	showuparrow
	inc	0x30
	jmp	Generator

notUp:
	call	showdownarrow
	inc	0x31
	jmp	Generator

; ------ Zufallszahlengenerator-----------------
RANDOM:	mov	A, RAND		;initialisiere A mit RAND
	jnz	RAB		;jump bei gelöschtem carry bit
	cpl	A		;complimentiere
	mov	RAND, A
RAB:	anl	a, #10111000b	;bitweises und (184d))
	mov	C, P
	mov	A, RAND
	rlc	A		;rotiere durch carry nach links
	mov	RAND, A
	ret
	

	
; --------- Arrows ----------------------------------
showUpArrow:
	cjne	R5, #01h, upArrow
	ret
showDownArrow:
	cjne	R5, #01h, downArrow
	ret
showLeftArrow:
	cjne	R5, #01h, leftArrow
	ret
showRightArrow:
	cjne	R5, #01h, rightArrow
	ret
upArrow:
	mov	b, #0b
	inc	R5
	call	resetPorts
	call	up
	call	up
	call	up
	call	up
	call	up
	jmp showUpArrow


downArrow:
	mov	b, #0b
	inc	R5
	call	resetPorts
	call	down
	call	down
	call	down
	call	down
	call	down
	jmp showDownArrow

leftArrow:
	mov	b, #0b
	inc	R5
	call	resetPorts
	call	left
	call	left
	call	left
	call	left
	call	left
	jmp showLeftArrow



rightArrow:
	mov	b, #0b
	inc	R5
	call	resetPorts
	call	right
	call	right
	call	right
	call	right
	call	right
	jmp showRightArrow

up:
	call	portleftup
	call	portArrow
	call	displayVertical
	ret

down:
	call	portrightdown
	call	portArrow
	call	displayVertical
	ret

right:
	call	portrightdown
	call	portArrow
	call	displayHorizontal
	ret

left:
	call	portleftup
	call	portArrow
	call	displayHorizontal
	ret

portArrow:
	mov	DPTR, #arrows
	mov	a, b
	movc	a, @a+dptr	
	mov	R3, a
	inc	b
	ret

portRightDown:
	mov	DPTR, #rightDown
	mov	a, b
	movc	a, @a+dptr
	mov	R2, a
	ret

portLeftUp:
	mov	DPTR, #leftUp
	mov	a, b
	movc	a, @a+dptr
	mov	R2, a
	ret

resetPorts:
	mov	P3, #0ffh
	mov	P2, #0ffh
	ret
displayHorizontal:
	call	resetports
	mov	P3, R3
	mov	P2, R2
	ret

displayVertical:
	call	resetports
	mov	P2, R3
	mov	P3, R2
	ret
	
;---- vordefinierte Lieder der Datenbank (1 = up, 2 = down, 3 = left, 4 = right)--------
moveIt: 
	db 	4h, 1h, 4h, 1h, 2h, 2h, 2h, 2h, 2h, 1h, 2h, 2h, 1h, 3h, 3h, 4h, 4h, 1h, 2h, 3h, 4h, 1h, 2h, 3h, 4h, 1h, 4h, 3h, 4h, 3h, 3h, 1h, 1h 
;------ Muster für die Ports -------
arrows:
	db	11100111b, 00000000b, 10000001b, 11000011b, 11100111b
leftUp:
	db	00001111b, 11110111b, 11111011b, 11111101b, 11111110b
rightDown:
	db	11110000b, 11101111b, 11011111b, 10111111b, 01111111b

	 	
	end
